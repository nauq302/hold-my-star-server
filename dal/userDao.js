const mongoose = require('mongoose')

const Users = mongoose.Schema({
    username: String,
    password: String,
})

const AccountModel = mongoose.model('users', Users)

async function login(username, password) {
    const query = AccountModel.findOne({ username: username})

    let account = null
    let error = null

    try {
        let acc = await query.exec()

        if (!acc) {
            error = 'No account found'
            console.log(error)

        } else if (acc.password !== password) {
            error = 'Wrong password'
            console.log(error)
            
        } else {
            account = acc
        }

        
    } catch (err) {   
        console.log(err)
        error = err
    }


    return { account, error }
}

async function signup(username, password, email) {
    const checkExistQuery = AccountModel.findOne({$or: [
        { username },
        { email }
    ]})

    const createQuery = AccountModel.create({
        username,
        password,
        email
    })


    let account = null
    let error = null

    try {
        let acc = await checkExistQuery.exec()

        if (acc) {
            error = 'Account with username or email already exists'
            console.log(error)
            return { error }
        } 

        acc = await createQuery.exec()


        
        
    } catch (err) {   
        console.log(err)
        error = err
        return { error }
    }


}


module.exports = { login, signup }