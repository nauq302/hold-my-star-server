const mongoose = require('mongoose')

const DB_NAME = 'HoldMyStar'

console.log('Connecting to database...')
mongoose.connect(`mongodb://localhost:27017/${DB_NAME}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

