var express = require('express');
var router = express.Router();

var userDao = require('../dal/userDao')


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/login', async (req, res) => {

    const { account, error } = await userDao.login(req.body.username, req.body.password)

    if (error) {
        res.status(400).json({ error })

    } else {
        res.status(200).json({ account })
    }


});


router.post('/signup', async (req, res) => {

    const { account, error } = await userDao.signup(req.body.username, req.body.password)

    if (error) {
        res.status(400).json({ error })

    } else {
        res.status(200).json({ account })
    }


});

module.exports = router;
